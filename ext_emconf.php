<?php

$EM_CONF['recipe'] = [
    'title' => 'Recipe collection',
    'description' => 'Recipe administration plugin for TYPO3, Pure BE editing.',
    'category' => 'fe',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Jan Helke',
    'author_email' => 'info@quintanion.com',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.6.0-8.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

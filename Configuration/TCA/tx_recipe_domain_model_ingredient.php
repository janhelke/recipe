<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$ll = 'LLL:EXT:recipe/Resources/Private/Language/locallang_db.xlf:';

return [
    'ctrl' => [
        'title' => $ll . 'tx_recipe_domain_model_ingredient',
        'label' => 'ingredient',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'default_sortby' => 'ORDER BY ingredient',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'hideTable' => true,
        'iconfile' => 'EXT:recipe/Resources/Public/Icons/tx_recipe_domain_model_ingredient.svg',
        'searchFields' => 'uid,ingredient',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, ingredient, amount, unit',
    ],
    'types' => [
        '0' => [
            'showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, ingredient, amount, unit,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden',
        ],
    ],
    'columns' => [
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ],
        ],
        'ingredient' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_ingredient.ingredient',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'required',
            ],
        ],
        'amount' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_ingredient.amount',
            'config' => [
                'type' => 'input',
                'size' => 30,
            ],
        ],
        'unit' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_ingredient.unit',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_recipe_domain_model_unit',
                'size' => '1',
            ],
        ],
    ]
];

<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$ll = 'LLL:EXT:recipe/Resources/Private/Language/locallang_db.xlf:';

return [
    'ctrl' => [
        'title' => $ll . 'tx_recipe_domain_model_cook',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'default_sortby' => 'ORDER BY name',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'iconfile' => 'EXT:recipe/Resources/Public/Icons/tx_recipe_domain_model_cook.svg',
        'searchFields' => 'uid,name, email, twitter, url',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, name, email, twitter, url',
    ],
    'types' => [
        '0' => [
            'showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, name, email, twitter, url,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden',
        ],
    ],
    'columns' => [
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ],
        ],
        'name' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_cook.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'required',
            ],
        ],
        'email' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_cook.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
            ],
        ],
        'twitter' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_cook.twitter',
            'config' => [
                'type' => 'input',
                'size' => 30,
            ],
        ],
        'url' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_cook.url',
            'config' => [
                'type' => 'input',
                'size' => 30,
            ],
        ],
    ]
];

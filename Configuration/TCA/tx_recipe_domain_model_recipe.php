<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$ll = 'LLL:EXT:recipe/Resources/Private/Language/locallang_db.xlf:';

return [
    'ctrl' => [
        'title' => $ll . 'tx_recipe_domain_model_recipe',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'default_sortby' => 'ORDER BY title',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'iconfile' => 'EXT:recipe/Resources/Public/Icons/tx_recipe_domain_model_recipe.svg',
        'searchFields' => 'uid,name,description,instruction',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, title, subtitle, description, instruction, images, teaser_image, ingredients, cook',
    ],
    'types' => [
        '0' => [
            'showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, title, subtitle, description, instruction, images, teaser_image, ingredients, cook,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden, --palette--;;1, starttime, endtime',
        ],
    ],
    'columns' => [
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ],
        ],
        'starttime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'title' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'required',
            ],
        ],
        'subtitle' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'required',
            ],
        ],
        'description' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.description',
            'config' => [
                'type' => 'text',
                'eval' => 'required',
            ],
        ],
        'instruction' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.instruction',
            'config' => [
                'type' => 'text',
                'eval' => 'required',
            ],
        ],
        'images' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('images',
                [
                    // Use the imageoverlayPalette instead of the basicoverlayPalette
                    'foreign_types' => [
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                    ],
                    'appearance' => [
                        'collapseAll' => true,
                        'expandSingle' => false,
                    ],
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'teaser_image' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.teaser_image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('teaser_image',
                [
                    'maxitems' => 1,
                    'foreign_match_fields' => [
                        'fieldname' => 'teaser_image',
                        'tablenames' => 'tx_recipe_domain_model_recipe',
                        'table_local' => 'sys_file',
                    ],
                    // Use the imageoverlayPalette instead of the basicoverlayPalette
                    'foreign_types' => [
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette',
                        ],
                    ],
                    'appearance' => [
                        'collapseAll' => true,
                        'expandSingle' => false,
                    ],
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'ingredients' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.ingredients',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_recipe_domain_model_ingredient',
                'foreign_field' => 'recipe',
                'appearance' => [
                    'collapseAll' => true,
                    'expandSingle' => false,
                    'levelLinksPosition' => 'both'
                ],
            ],
        ],
        'cook' => [
            'exclude' => 0,
            'label' => $ll . 'tx_recipe_domain_model_recipe.cook',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_recipe_domain_model_cook',
                'size' => '1',
            ],
        ],
    ]
];



<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    [
        'Recipe controller',
        'recipe_recipe',
        'EXT:recipe/ext_icon.svg',
    ], 'CType', 'recipe'
);

$GLOBALS['TCA']['tt_content']['types']['recipe'] = $GLOBALS['TCA']['tt_content']['types']['header'];
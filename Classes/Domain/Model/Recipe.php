<?php

declare (strict_types = 1);

namespace Quintanion\Recipe\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Recipe extends AbstractEntity
{
    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $subtitle = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var string
     */
    protected $instruction = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $images;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $teaserImage;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Quintanion\Recipe\Domain\Model\Ingredient>
     */
    protected $ingredients;

    /**
     * @var \Quintanion\Recipe\Domain\Model\Cook
     */
    protected $cook;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle($subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getInstruction(): string
    {
        return $this->instruction;
    }

    /**
     * @param string $instruction
     */
    public function setInstruction($instruction): void
    {
        $this->instruction = $instruction;
    }

    /**
     * @return ObjectStorage
     */
    public function getImages(): ObjectStorage
    {
        return $this->images;
    }

    /**
     * @param ObjectStorage $images
     */
    public function setImages($images): void
    {
        $this->images = $images;
    }

    /**
     * @return FileReference
     */
    public function getTeaserImage(): FileReference
    {
        return $this->teaserImage;
    }

    /**
     * @param FileReference $teaserImage
     */
    public function setTeaserImage($teaserImage): void
    {
        $this->teaserImage = $teaserImage;
    }

    /**
     * @return ObjectStorage
     */
    public function getIngredients(): ObjectStorage
    {
        return $this->ingredients;
    }

    /**
     * @param ObjectStorage $ingredients
     */
    public function setIngredients($ingredients): void
    {
        $this->ingredients = $ingredients;
    }

    /**
     * @return Cook
     */
    public function getCook(): Cook
    {
        return $this->cook;
    }

    /**
     * @param Cook $cook
     */
    public function setCook($cook): void
    {
        $this->cook = $cook;
    }


}

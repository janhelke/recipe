<?php

declare (strict_types = 1);

namespace Quintanion\Recipe\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Ingredient extends AbstractEntity
{
    /**
     * @var string
     */
    protected $amount = '';

    /**
     * @var string
     */
    protected $ingredient = '';

    /**
     * @var \Quintanion\Recipe\Domain\Model\Unit
     */
    protected $unit;

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getIngredient(): string
    {
        return $this->ingredient;
    }

    /**
     * @param string $ingredient
     */
    public function setIngredient($ingredient): void
    {
        $this->ingredient = $ingredient;
    }

    /**
     * @return Unit
     */
    public function getUnit(): Unit
    {
        return $this->unit;
    }

    /**
     * @param Unit $unit
     */
    public function setUnit($unit): void
    {
        $this->unit = $unit;
    }
}

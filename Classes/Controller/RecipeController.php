<?php

declare(strict_types=1);

namespace Quintanion\Recipe\Controller;

use Quintanion\Recipe\Domain\Model\Recipe;
use Quintanion\Recipe\Domain\Repository\RecipeRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class RecipeController extends ActionController
{

    /**
     * @var RecipeRepository
     */
    protected $recipeRepository;

    public function listAction(): void
    {
        $this->view->assignMultiple(
            [
                'recipes' => $this->recipeRepository->findAll()
            ]
        );
    }

    public function showAction(Recipe $recipe): void
    {
        $this->view->assignMultiple(
            [
                'recipe' => $recipe
            ]
        );
    }

    /**
     * @param RecipeRepository $recipeRepository
     */
    public function injectRecipeRepository(RecipeRepository $recipeRepository): void
    {
        $this->recipeRepository = $recipeRepository;
    }
}